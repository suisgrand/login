﻿using System;
using data.model;
using Microsoft.EntityFrameworkCore;

namespace data.persistence
{
    public class TaskDbContext : DbContext
    {

        public DbSet<User> Users { get; set; }
        public DbSet<ReasonItem> ReasonItems { get; set; }
        public DbSet<ReasonCustomerSignUp> ReasonCustomerSignUps { get; set; }

        public TaskDbContext(DbContextOptions<TaskDbContext> options)
        : base(options)
        {

        }
    }
}
