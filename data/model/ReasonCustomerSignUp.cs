﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace data.model
{
    [Table("ReasonCustomerSignUps")]
    public class ReasonCustomerSignUp
    {
        [Key]
        public int id { get; set; }
        [StringLength(50)]
        public string OptionalAnswer { get; set; }
        public int UserId { get; set; }
        [ForeignKey("ReasonItem")]
        public int ReasonItemId { get; set; }
    }
}
