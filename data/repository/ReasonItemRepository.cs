﻿using System.Collections.Generic;
using System.Threading.Tasks;
using data.contract;
using data.model;
using data.persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace data.repository
{
    public class ReasonItemRepository : IReasonItemRepository
    {
        private readonly TaskDbContext _context;
        private readonly ILogger<ReasonItemRepository> _logger;

        public ReasonItemRepository(TaskDbContext context, ILogger<ReasonItemRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<IEnumerable<ReasonItem>> GetReasonItems()
        {
            _logger.LogInformation("Get all reason items");
            return await _context.ReasonItems.ToListAsync();
        }

        public async Task<bool> CheckIfReasonItemExists(int reasonId)
        {
            return await _context.ReasonItems.AnyAsync(x => x.Id == reasonId);
        }

    }
}
