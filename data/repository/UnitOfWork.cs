﻿using System.Threading.Tasks;
using data.contract;
using data.persistence;
using Microsoft.EntityFrameworkCore.Storage;

namespace data.repository
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly TaskDbContext _context;
        public UnitOfWork(TaskDbContext context)
        {
            _context = context;
        }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync(default);
        }

        public IDbContextTransaction contextTransation()
        {
            return _context.Database.BeginTransaction();
        }
    }
}
