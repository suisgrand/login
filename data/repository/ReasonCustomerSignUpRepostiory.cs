﻿using System;
using data.contract;
using data.model;
using data.persistence;
using Microsoft.Extensions.Logging;

namespace data.repository
{
    public class ReasonCustomerSignUpRepostiory: IReasonCustomerSignUpRepostiory
    {
        private readonly TaskDbContext _context;
        private readonly ILogger<ReasonCustomerSignUpRepostiory> _logger;

        public ReasonCustomerSignUpRepostiory(TaskDbContext context, ILogger<ReasonCustomerSignUpRepostiory> logger)
        {
            _context = context;
            _logger = logger;
        }

        public void Add(ReasonCustomerSignUp reasonSignUp)
        {
            _logger.LogInformation($"ReasonCustomerSignUpRepostiory.Add(): {reasonSignUp}");
            _context.ReasonCustomerSignUps.Add(reasonSignUp);
        }

    }
}
