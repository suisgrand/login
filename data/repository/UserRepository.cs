﻿using System;
using System.Threading.Tasks;
using data.contract;
using data.model;
using data.persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace data.repository
{
    public class UserRepository : IUserRepository
    {
        private readonly TaskDbContext _context;
        private readonly ILogger<UserRepository> _logger;

        public UserRepository(TaskDbContext context, ILogger<UserRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public void Add(User user)
        {
            _logger.LogInformation($"UserRepository.Add(): {user}");
            _context.Add(user);
        }

        public async Task<bool> CheckIfEmailExists(string email)
        {
            _logger.LogInformation($"UserRepository.CheckIfEmailExists: {email}");
            return await _context.Users.
                AnyAsync(x => x.Email.Trim().Equals(email.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
