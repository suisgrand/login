﻿using System;
using System.Threading.Tasks;
using data.model;

namespace data.contract
{
    public interface IUserRepository
    {
        void Add(User user);

        Task<bool> CheckIfEmailExists(string email);
    }
}
