﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using data.model;

namespace data.contract
{
    public interface IReasonItemRepository
    {
        Task<IEnumerable<ReasonItem>> GetReasonItems();

        Task<bool> CheckIfReasonItemExists(int reasonId);
    }
}
