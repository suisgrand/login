﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace data.contract
{
    public interface IUnitOfWork
    {
        Task CompleteAsync();

        IDbContextTransaction contextTransation();
    }
}
