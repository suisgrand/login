﻿using System;
using NUnit.Framework;
using Moq;
using Microsoft.Extensions.Logging;
using api.Controllers;
using service.contract;
using presentation.mapping;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using presentation.model;
using Helper;

namespace test
{
    [TestFixture]
    public class PresentationLayerTest
    {

        Mock<ILogger<SignUpController>> _logger;
        Mock<IReasonItemService> _reasonItemService;
        Mock<ISignUpService> _signUpService;
        SignUpController _controller;

        [SetUp]
        public void SetUp()
        {
            _logger = new Mock<ILogger<SignUpController>>();
            _reasonItemService = new Mock<IReasonItemService>();
            _signUpService = new Mock<ISignUpService>();

            var myProfile = new MappingProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            _controller = new SignUpController(_reasonItemService.Object, _logger.Object, _signUpService.Object, mapper);
        }


        [Test]
        public void SimpleTest()
        {
            Assert.True(true);
        }


        [Test]
        public void GetReasonItems_Success()
        {
            //Arrange
            var ls = new List<data.model.ReasonItem> {
                new data.model.ReasonItem {Id = 1, Description = "Advert"},
                new data.model.ReasonItem {Id = 2, Description = "Word of Mouth"},
                new data.model.ReasonItem {Id = 3, Description = "Other"} };

            var test = ls.AsEnumerable();

            _reasonItemService.Setup(x => x.GetReasonItems()).ReturnsAsync(ls);

            //ACT
            var actionResult = _controller.GetReasonItems().Result as OkObjectResult;
            var obj = actionResult as OkObjectResult;

            //Assert
            Assert.NotNull(obj);
        }

        [Test]
        public void GetReasonItems_Failure()
        {
            //Arrange
            var ls = new List<data.model.ReasonItem> {
                new data.model.ReasonItem {Id = 1, Description = "Advert"},
                new data.model.ReasonItem {Id = 2, Description = "Word of Mouth"},
                new data.model.ReasonItem {Id = 3, Description = "Other"} };

            var test = ls.AsEnumerable();

            _reasonItemService.Setup(x => x.GetReasonItems()).Throws(new Exception());

            //ACT
            var actionResult = _controller.GetReasonItems().Result;
            var status = actionResult as StatusCodeResult;

            //Assert
            Assert.That(status.StatusCode == 500);
        }

        [Test]
        public void CreateNewUser_Success()
        {
            //Arrange
            var user = new SignUpForm { Email = "stephane.verger@gov.ie", ReasonItemId = 1 };
            _signUpService.Setup(x => x.CreateNewUser(It.IsAny<service.model.SignUpForm>()))
                .ReturnsAsync(StatusCodes.OK);

            //act
            var result = _controller.CreateUser(user).Result as StatusCodeResult;

            Assert.That(result.StatusCode == 200);
        }

        [Test]
        public void CreateNewUser_Email_Incorrect()
        {
            //Arrange
            var user = new SignUpForm { Email = "stephane.verger@gov.ie", ReasonItemId = 1 };
            _signUpService.Setup(x => x.CreateNewUser(It.IsAny<service.model.SignUpForm>()))
                .ReturnsAsync(StatusCodes.BadData);

            //act
            var result = _controller.CreateUser(user).Result as StatusCodeResult;

            Assert.That(result.StatusCode == 400);
        }

        [Test]
        public void CreateNewUser_EmailFound()
        {
            //Arrange
            var user = new SignUpForm { Email = "stephane.verger@gov.ie", ReasonItemId = 1 };
            _signUpService.Setup(x => x.CreateNewUser(It.IsAny<service.model.SignUpForm>()))
                .ReturnsAsync(StatusCodes.Conflict);

            //act
            var result = _controller.CreateUser(user).Result as StatusCodeResult;

            Assert.That(result.StatusCode == 409);
        }


    }
}
