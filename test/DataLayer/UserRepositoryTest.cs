
using System;
using System.Threading.Tasks;
using data.model;
using data.repository;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using test.helper;

namespace test.DataLayer
{
    public class UserRepositoryTest
    {
        Mock<ILogger<UserRepository>> _logger;
        UserRepository _repository;

        TestDbContext _context;

        [SetUp]
        public void Setup()
        {
            _context = new TestDbContext();
            _logger = new Mock<ILogger<UserRepository>>();
            _repository = new UserRepository(_context.GetDbContext(), _logger.Object);
        }

        [Test]
        public void Add_Test()
        {
            //arrange
            var user = new User{
                Email = "email"
            };
            //act assert
            Assert.DoesNotThrow(() => _repository.Add(user));
        }

        

        [TestCase("my-email", true)]
        [TestCase("new", false)]
        public async Task CheckIfEmailExists_Test(string email, bool doesExist)
        {
            //arrange
            _context.AddUsers();
            //act
            var result = await _repository.CheckIfEmailExists(email);

            Assert.AreEqual(result, doesExist);
        }


    }
}