﻿using System;
using System.Linq;
using System.Threading.Tasks;
using data.model;
using data.repository;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using test.helper;

namespace test
{
    public class ReasonCustomerSignUpRepostioryTest
    {
        Mock<ILogger<ReasonCustomerSignUpRepostiory>> _logger;
        ReasonCustomerSignUpRepostiory _repository;

        TestDbContext _context;

        [SetUp]
        public void Setup()
        {
            _context = new TestDbContext();
            _logger = new Mock<ILogger<ReasonCustomerSignUpRepostiory>>();
            _repository = new ReasonCustomerSignUpRepostiory(_context.GetDbContext(), _logger.Object);
        }

        [Test]
        public void Add_Test()
        {
            //arrange
            var reasonCustomerSignUp = new ReasonCustomerSignUp();

            //act assert
            Assert.DoesNotThrow(() => _repository.Add(reasonCustomerSignUp));
        }
    }
}
