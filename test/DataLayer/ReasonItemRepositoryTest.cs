using System;
using System.Linq;
using System.Threading.Tasks;
using data.repository;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using test.helper;


namespace test.DataLayer
{
    public class ReasonItemRepositoryTest
    {
        Mock<ILogger<ReasonItemRepository>> _logger;
        ReasonItemRepository _repository;

        TestDbContext _context;

        [SetUp]
        public void Setup()
        {
            _context = new TestDbContext();
            _logger = new Mock<ILogger<ReasonItemRepository>>();
            _repository = new ReasonItemRepository(_context.GetDbContext(), _logger.Object);
        }

        [Test]
        public async Task GetReasonItems_Test()
        {
            //Arrange
            _context.AddReasonItems();

            //Act
            var result = await _repository.GetReasonItems();

            //Assert
            Assert.True(result.Any());
        }


        [TestCase("1", true)]
        [TestCase("4", false)]
        public async Task CheckIfReasonItemExists_Test(int id, bool expectedResult)
        {
            //Arrange
            _context.AddReasonItems();
            //Act
            var result = await _repository.CheckIfReasonItemExists(id);

            Assert.AreEqual(result, expectedResult);
        }


    }
}