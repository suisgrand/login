﻿using System;
using System.Collections.Generic;
using System.Linq;
using data.model;
using data.persistence;
using Microsoft.EntityFrameworkCore;

namespace test.helper
{
    public class TestDbContext
    {

        private TaskDbContext context { get; set; }
        public TaskDbContext GetDbContext()
        {
            var options = new DbContextOptionsBuilder<TaskDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            context = new TaskDbContext(options);
            context.Database.EnsureCreated();

            return context;
        }

        public void AddUsers()
        {
            context.Users.Add(new User { Email = "my-email" });
            context.SaveChanges();
        }

        public void AddReasonItems()
        {
            context.ReasonItems.Add(new ReasonItem { Id = 1, Description = "item 1"});
            context.ReasonItems.Add(new ReasonItem { Id = 2, Description = "item 2"});
            context.ReasonItems.Add(new ReasonItem { Id = 3, Description = "item 3"});

            context.SaveChanges();
        }

    }
}
