﻿using System;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading.Tasks;
using System.Threading;

namespace test
{
    public partial class ServiceLayerTest
    {
        class DbContextTransaction : IDbContextTransaction
        {
            public Guid TransactionId => throw new NotImplementedException();

            public void Commit()
            {
                
            }

            public Task CommitAsync(CancellationToken cancellationToken = default)
            {
                throw new NotImplementedException();
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public ValueTask DisposeAsync()
            {
                throw new NotImplementedException();
            }

            public void Rollback()
            {

            }

            public Task RollbackAsync(CancellationToken cancellationToken = default)
            {
                throw new NotImplementedException();
            }
        }

    }
}
