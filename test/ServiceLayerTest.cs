﻿using AutoMapper;
using data.contract;
using data.repository;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using service.classes;
using Helper;
using service.ServiceHelper;
using data.persistence;
using System.Threading.Tasks;
using System.Collections.Generic;
using api.model;
using System.Linq;
using presentation.mapping;

namespace test
{
    [TestFixture]
    public partial class ServiceLayerTest
    {
        Mock<ILogger<SignUpService>> _logger;
        SignUpService _signUpService;
        Mock<IUserRepository> _repositoryUser;
        Mock<IReasonItemRepository> _repositoryReasonItem;
        Mock<IReasonCustomerSignUpRepostiory> _repositorySignUpReason;
        Mock<IUnitOfWork> _uow;

        ReasonItemService _reasonItemService;


        [SetUp]
        public void SetUp()
        {
            _logger = new Mock<ILogger<SignUpService>>();

            _repositoryUser = new Mock<IUserRepository>();
            _repositoryReasonItem = new Mock<IReasonItemRepository>();
            _repositorySignUpReason = new Mock<IReasonCustomerSignUpRepostiory>();

            _uow = new Mock<IUnitOfWork>();

            var myProfile = new MappingProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);

            _signUpService = new SignUpService(_uow.Object,
                _repositoryUser.Object,
                _repositoryReasonItem.Object,
                _repositorySignUpReason.Object,
                _logger.Object,
                mapper);

            _reasonItemService = new ReasonItemService(_repositoryReasonItem.Object);

        }

        [Test]
        public async Task GetReasonItems_Test()
        {
            //Arrange
            var ls = new List<data.model.ReasonItem> {
                new data.model.ReasonItem {Id = 1, Description = "Advert"},
                new data.model.ReasonItem {Id = 2, Description = "Word of Mouth"},
                new data.model.ReasonItem {Id = 3, Description = "Other"} };

            _repositoryReasonItem.Setup(x => x.GetReasonItems()).ReturnsAsync(ls);

            //Act
            var result = await _reasonItemService.GetReasonItems();

            //Assert
            Assert.AreEqual(result.Count(), ls.Count);
        }


        [TestCase("stephane.verger@france.ie", true)]
        [TestCase("stephane@france", false)]
        [TestCase("stephane@france.ie", true)]
        public void testEmail(string email, bool trueOrFalse)
        {
            var result = email.IsEmailCorrect();

            Assert.That(result == trueOrFalse);
        }

        [TestCase("stephane.verger@dgse.com", false, true, StatusCodes.OK)]
        [TestCase("stephane.verger@dgse.com", true, true, StatusCodes.Conflict)]
        [TestCase("stephane.verger@dgse", false, true, StatusCodes.BadData)]
        [TestCase("stephane.verger@dgse", false, true, StatusCodes.Error)]
        public async Task CreateNewUser_Test(string email, bool doesEmailExist, bool isReasonIdFound, StatusCodes expectedResult)
        {
            //Arrange
            var signUpForm = new service.model.SignUpForm {
                Email = email,
                SigninReasonDescription = string.Empty,
                ReasonItemId = 1
            };

            if(expectedResult == StatusCodes.Error){
                _repositoryUser.Setup(x => x.CheckIfEmailExists(signUpForm.Email))
                .Throws(new System.Exception());
            }
            else{
                _repositoryUser.Setup(x => x.CheckIfEmailExists(signUpForm.Email))
                .ReturnsAsync(doesEmailExist);
            }
            
            _repositoryReasonItem.Setup(x => x.CheckIfReasonItemExists(signUpForm.ReasonItemId))
                .ReturnsAsync(isReasonIdFound);

            _uow.Setup(x => x.contextTransation()).Returns(new DbContextTransaction());
            _uow.Setup(x => x.CompleteAsync());

            //Assert
            var result = await _signUpService.CreateNewUser(signUpForm);

            Assert.AreEqual(expectedResult, result);
        }

    }
}
