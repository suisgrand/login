﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using data.model;

namespace service.contract
{
    public interface IReasonItemService
    {
        Task<IEnumerable<ReasonItem>> GetReasonItems();
    }
}
