using System.Collections.Generic;
using System.Threading.Tasks;
using Helper;
using service.model;

namespace service.contract
{
    public interface ISignUpService
    {
        Task<StatusCodes> CreateNewUser(SignUpForm signUpForm);
    }
}

