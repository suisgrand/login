namespace service.model 
{
    public class SignUpForm
    {
        public string Email { get; set; }
        public string SigninReasonDescription { get; set; }
        public int ReasonItemId { get; set; }
    }    
}