﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using data.contract;
using data.model;
using service.contract;

namespace service.classes
{
    public class ReasonItemService: IReasonItemService
    {
        private readonly IReasonItemRepository _repository;

        public ReasonItemService(IReasonItemRepository repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<ReasonItem>> GetReasonItems()
        {
            return await _repository.GetReasonItems();
        }
    }
}
