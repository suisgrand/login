using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using data.contract;
using data.model;
using Helper;
using Microsoft.Extensions.Logging;
using service.contract;
using service.model;
using service.ServiceHelper;

namespace service.classes
{
    public class SignUpService: ISignUpService
    {
        private readonly IUnitOfWork _uow;
        private readonly IUserRepository _repositoryUser;
        private readonly IReasonItemRepository _repositoryReasonItem;
        private readonly IReasonCustomerSignUpRepostiory _repositorySignUpReason;
        private readonly IMapper _mapper;
        private ILogger<SignUpService> _logger;

        public SignUpService(IUnitOfWork uow,
            IUserRepository repositoryUser,
            IReasonItemRepository repositoryReasonItem,
            IReasonCustomerSignUpRepostiory repositorySignUpReason,
            ILogger<SignUpService> logger,
            IMapper mapper)
        {
            _uow = uow;
            _repositoryUser = repositoryUser;
            _repositoryReasonItem = repositoryReasonItem;
            _repositorySignUpReason = repositorySignUpReason;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<StatusCodes> CreateNewUser(SignUpForm signUpForm)
        {
            _logger.LogInformation($"CreateNewUser: {signUpForm}");
            var transaction = _uow.contextTransation();
            var statusCode = StatusCodes.Error;

            try
            {
                using (transaction)
                {
                    var user = _mapper.Map<SignUpForm, User>(signUpForm);

                    if (await _repositoryUser.CheckIfEmailExists(user.Email))
                    {
                        statusCode = StatusCodes.Conflict;
                        throw new System.Exception();
                    }

                    if(! user.Email.IsEmailCorrect())
                    {
                        statusCode = StatusCodes.BadData;
                        throw new System.Exception();
                    }

                    _repositoryUser.Add(user);
                    await _uow.CompleteAsync();

                    var reasonItem = _mapper.Map<SignUpForm, ReasonItem>(signUpForm);
                    if(!await _repositoryReasonItem.CheckIfReasonItemExists(reasonItem.Id))
                    {
                        statusCode = StatusCodes.BadData;
                        throw new System.Exception();
                    }

                    var reasonSignUp = _mapper.Map<SignUpForm, ReasonCustomerSignUp>(signUpForm);
                    _repositorySignUpReason.Add(reasonSignUp);

                    await _uow.CompleteAsync();


                    transaction.Commit();
                    statusCode = StatusCodes.OK;
                }
            }
            catch (System.Exception ex)
            {
                transaction.Rollback();
                _logger.LogError("CreateNewUser error: " + ex);
            }


            return statusCode;
        }
    }
}
