import { SignUpForm } from './../model/SignUpForm';
import { HttpClient } from '@angular/common/http';
import { Injectable, Inject, InjectionToken } from '@angular/core';
import { Observable} from 'rxjs';
import { ReasonItem } from '../model/ReasonItem';

import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(private http: HttpClient) { }

  GetReasonItems(): Observable<ReasonItem[]> {
    console.log('service');
    const url = 'https://localhost:5001/SignUp/reasons';
    return this.http.get<ReasonItem[]>(url);
  }

  createUser(user: SignUpForm) {
    const url = 'https://localhost:5001/SignUp';
    return this.http.post(url, user, { observe: 'response' })
      .pipe(
        map((res: any) => {
          return res.status;
        }),
        catchError(error => {
          console.log('error', error);
          return of(error.status);
        })
      );

      // .subscribe(
      //   (response) => {
      //     console.log('response received');
      //     return response.status;
      //   },
      //   (error) => {
      //     console.error('error caught in component', error);
      //     return error.status;
      //   });
  }

}
