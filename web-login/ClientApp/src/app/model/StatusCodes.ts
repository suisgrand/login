
export enum StatusCodes {
    Ok = 200,
    BadData = 400,
    Conflict = 409,
    ServerError = 500
}
