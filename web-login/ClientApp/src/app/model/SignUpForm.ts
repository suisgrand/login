
export interface SignUpForm {
    email: string;
    signinReasons: string;
    reasonItemId: number;
}
