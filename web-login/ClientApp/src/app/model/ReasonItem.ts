

export interface ReasonItem {
    id: number;
    description: string;
}
