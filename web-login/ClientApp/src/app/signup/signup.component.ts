import { SignUpForm } from './../model/SignUpForm';
import { ReasonItem } from './../model/ReasonItem';
import { Component, OnInit } from '@angular/core';
import { SignupService } from '../service/signup.service';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { StatusCodes } from '../model/StatusCodes';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  form: FormGroup;
  reasonItems: ReasonItem[] = [];
  signUpForm: SignUpForm;
  bannerDescription: string;
  bannerClass: string;
  showBanner: boolean;


  constructor(private signUpService: SignupService, private fb: FormBuilder) { }

  ngOnInit(): void {

    this.form = this.fb.group({
      email: new FormControl('', [Validators.email, Validators.required]),
      signupreason: new FormControl(),
      reasonItems: ['', Validators.required]
    });

    this.signUpService.GetReasonItems().subscribe(data => {
      this.reasonItems = data;
    });

  }

  get email() {
    return this.form.get('email');
  }

  get reasons() {
    return this.form.get('reasonItems');
  }


  signUp() {
    this.showBanner = false;
    const email = this.form.get('email').value;
    const reasonId = Number(this.form.get('reasonItems').value);

    if (!this.form.get('email').invalid && this.reasonItems.find(x => x.id === reasonId)) {
      this.signUpForm = {
        email: email,
        signinReasons: this.form.get('signupreason').value,
        reasonItemId: this.form.get('reasonItems').value
      };
      this.postSignUp();
    }

    if (reasonId === 0) {
      this.form.get('reasonItems').markAsTouched();
    }

    if (this.form.get('email').invalid) {
      this.form.get('email').markAsTouched();
    }

  }

  private postSignUp() {
    this.signUpService.createUser(this.signUpForm).subscribe(result => {
      console.log('result post', result);
      this.showBanner = true;
      this.bannerClass = 'alert-danger';
      switch (result) {
        case StatusCodes.Ok:
          this.bannerClass = 'alert-success';
          this.bannerDescription = 'You have been signed up now';
          break;
        case StatusCodes.BadData:
        case StatusCodes.ServerError:
          this.bannerClass = 'alert-danger';
          this.bannerDescription = 'there is a problem with the application';
          break;
        case StatusCodes.Conflict:
          this.bannerClass = 'alert-warning';
          this.bannerDescription = 'you have already signed up';
          break;
        default:
          this.bannerDescription = '';
          this.showBanner = false;
      }

    });
  }

}
