

using System;
using System.Collections.Generic;
using AutoMapper;
using data.contract;
using data.model;
using data.persistence;
using data.repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using service.classes;
using service.contract;

namespace login_form
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors();
            
            services.AddDbContext<TaskDbContext>(opt => {
                    opt.UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString());
                    opt.ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning));
                },
                ServiceLifetime.Singleton, ServiceLifetime.Singleton);

            services.AddAutoMapper(typeof(Startup));

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IReasonItemRepository, ReasonItemRepository>();
            services.AddScoped<IReasonItemRepository, ReasonItemRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IReasonCustomerSignUpRepostiory, ReasonCustomerSignUpRepostiory>();


            services.AddScoped<ISignUpService, SignUpService>();
            services.AddScoped<IReasonItemService, ReasonItemService>();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "login_form", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(
                options => options.WithOrigins("https://localhost:8520", "http://localhost:37890")
                .AllowAnyMethod()
                .AllowAnyHeader()                
            );

            var context = app.ApplicationServices.GetService<TaskDbContext>();
            populateReasonItems(context);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "login_form v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void populateReasonItems(TaskDbContext context)
        {
            var coll = new List<ReasonItem> {
                new ReasonItem {Id = 1, Description = "Advert"},
                new ReasonItem {Id = 2, Description = "Word of Mouth"},
                new ReasonItem {Id = 3, Description = "Other"}
            };

            foreach(var item in coll)
            {
                context.ReasonItems.Add(item);
            }
            context.SaveChanges();
        }
    }
}
