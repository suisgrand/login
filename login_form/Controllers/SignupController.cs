                                                                                
using Microsoft.AspNetCore.Mvc;
using service.contract;
using System.Collections.Generic;
using presentation.model;
using Microsoft.Extensions.Logging;
using AutoMapper;
using System.Threading.Tasks;
using api.model;
using Helper;

namespace api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SignUpController: ControllerBase
    {

        private readonly IReasonItemService _reasonItemService;
        private readonly ILogger _logger;
        private readonly ISignUpService _signUpService;
        private readonly IMapper _mapper;

        public SignUpController(
            IReasonItemService reasonItemService, 
            ILogger<SignUpController> logger,
            ISignUpService signUpService,
            IMapper mapper)
        {
            _reasonItemService = reasonItemService;
            _logger = logger;
            _signUpService = signUpService;
            _mapper = mapper;
        }


        [HttpGet("reasons")]
        public async Task<ActionResult> GetReasonItems()
        {
            _logger.LogInformation($"GetDiscoeryItems");
            try
            {
                var data = await _reasonItemService.GetReasonItems();
                var result = _mapper.Map<IEnumerable<data.model.ReasonItem>, IEnumerable<ReasonItem>>(data);
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                _logger.LogCritical($"GetDiscoeryItems error: {ex}");
                return StatusCode((int)StatusCodes.Error);
            }
            
        }

        [HttpPost]
        public async Task<ActionResult> CreateUser(SignUpForm userResource)
        {
            _logger.LogInformation($"CreateNewUser: {userResource}");
            var result = StatusCodes.None;
            try
            {
                var user = _mapper.Map<service.model.SignUpForm>(userResource);
                 result = await _signUpService.CreateNewUser(user);
            }
            catch (System.Exception ex)
            {
                _logger.LogCritical($"CreateNewUser error: {ex}");
                result = StatusCodes.Error;
            }

            return StatusCode((int)result);
        }

    }
}