

using System;
using System.Collections.Generic;
using AutoMapper;

namespace presentation.mapping
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<presentation.model.SignUpForm, service.model.SignUpForm>();

            CreateMap<data.model.ReasonItem, api.model.ReasonItem>();

            CreateMap<service.model.SignUpForm, data.model.User>()
                .ForMember(user => user.Email,
                    opt => opt.MapFrom(signUp => signUp.Email))
                .ForMember(user => user.Id, opt => opt.Ignore())
                .AfterMap((signUp, user) =>
                {
                    user.Id = 0;
                });

            CreateMap<service.model.SignUpForm, data.model.ReasonItem>()
                .ForMember(reasonItem => reasonItem.Id,
                    opt => opt.MapFrom(signUp => signUp.ReasonItemId));

            CreateMap<service.model.SignUpForm, data.model.ReasonCustomerSignUp>()
                .ForMember(reasonSignUp => reasonSignUp.OptionalAnswer,
                    opt => opt.MapFrom(signUp => signUp.SigninReasonDescription))
                .ForMember(reasonSignUp => reasonSignUp.ReasonItemId,
                    opt => opt.MapFrom(signUp => signUp.ReasonItemId));
        }


    }
}
