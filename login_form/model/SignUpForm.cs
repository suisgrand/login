

namespace presentation.model 
{
    public class SignUpForm
    {
        public string Email { get; set; }
        public string SigninReasons { get; set; }
        public int ReasonItemId { get; set; }
    }    
}
