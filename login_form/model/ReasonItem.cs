﻿using System;
namespace api.model
{
    public class ReasonItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
