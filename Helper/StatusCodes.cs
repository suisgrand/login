﻿using System;

namespace Helper
{
    public enum StatusCodes
    {
        None = 0,
        OK = 200,
        Error = 500,
        Conflict = 409,
        BadData = 400
    }
}
